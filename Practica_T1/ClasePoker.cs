﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Practica_T1
{
    public class ClasePoker
    {
        private List<Jugador> jugadores = new List<Jugador>(5);
        private List<string> cartasEnMesa = new List<string>();
        public String ganador { get; set; }

        public void iniciarJuego(List<Jugador> listaJugadores)
        {
            jugadores = listaJugadores;
            int canJugadores = jugadores.Count;
            if (2 <= canJugadores && canJugadores <= 5) Console.WriteLine("Juego Iniciado");
            throw new Exception("Para iniciar deben haber de 2 a 5 jugadores, no 1 ni más de 5");
        }

        public bool verificarBaraja(List<string> baraja)
        {
            if(baraja.Count >= 48 && baraja.Count <= 52)
            {
                List<string> contados = new List<string>();
                foreach (string i in baraja)
                {
                    if (contados.Contains(i))
                        throw new Exception("Hay cartas iguales, no se puede iniciar la partida");
                    else
                        contados.Add(i);
                }
                return true;
            }
            return false;
        }
        //verificará si esque una carta en mesa ya fue tirada con anterioridad
        //cartas mesa = cartas en la mesa, // cartasTiradas= cartas ya tiradas
        public bool verificarBarajaTiradas(List<string> cartasMesa, List<string> cartasTiradas){

            foreach (string carta in cartasMesa)
            {
                if (cartasMesa.Contains(carta))
                    throw new Exception("Hay una carta que ya fue tirada, tramposo");
            }

            return false;
        }
        public void retirarJugador(Jugador jugadorRetirar)
        {
            if(jugadores.Contains(jugadorRetirar))
                jugadores.Remove(jugadorRetirar);
            throw new Exception("Error al retirar, el jugador ya se ha retirado");
        }
        public void cartasComunitarias(List<string> cartasMesa)
        {
            cartasEnMesa = cartasMesa;
        }
        public string ganadorRonda()
        {
            return ganador;
        }
        public string ganadorJuego()
        {
            return ganador;
        }
    }

}
