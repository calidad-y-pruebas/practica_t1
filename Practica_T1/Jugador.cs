﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Practica_T1
{
    public class Jugador
    {
        public string nombre { get; set; }
        public List<string> cartas { get; set; }
        public string posicion { get; set; }
    }
}
