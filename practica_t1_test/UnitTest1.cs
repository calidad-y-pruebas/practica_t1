using NUnit.Framework;
using Practica_T1;
using System;
using System.Collections.Generic;

namespace practica_t1_test
{
    public class Tests
    {

        private ClasePoker poker = new ClasePoker();

        //Los casos de prueba deben validar que solo se pueda iniciar
        //un juego si se tiene minimo dos
        //jugadores(usar exceptions)
        [Test]
        public void Test1()
        {
            var prueba1 = new ClasePoker();

            Jugador jugador1 = new Jugador()
            {
                nombre = "Pablo",
                cartas = new List<string>()
                {
                    "1Trebol", "3Trebol", "10Corazones", "11Espadas", "12Cocos"
                }
            };
            List<Jugador> jugadores = new List<Jugador>();
            jugadores.Add(jugador1);

            Assert.Throws(typeof(Exception), () => prueba1.iniciarJuego(jugadores));
        }
        [Test]
        public void Test2()
        {
            var prueba1 = new ClasePoker();

            Jugador jugador1 = new Jugador()
            {
                nombre = "Pablo",
                cartas = new List<string>()
                {
                    "10Espada", "8Trebol", "10Corazon", "1Espada", "2Coco"
                }
            };
            Jugador jugador2 = new Jugador()
            {
                nombre = "Pablo",
                cartas = new List<string>()
                {
                    "5Espada", "8Coco", "1Corazon", "1Espada", "2Coco"
                }
            };
            List<Jugador> jugadores = new List<Jugador>();
            jugadores.Add(jugador1);
            jugadores.Add(jugador2);

            Assert.Throws(typeof(Exception), () => prueba1.iniciarJuego(jugadores));
        }
        [Test]
        public void Test3()
        {
            var prueba1 = new ClasePoker();

            Jugador jugador1 = new Jugador()
            {
                nombre = "Pablo",
                cartas = new List<string>()
                {
                    "10Espada", "8Trebol", "10Corazon", "2Espada", "1Coco"
                }
            };
            Jugador jugador2 = new Jugador()
            {
                nombre = "Pablo",
                cartas = new List<string>()
                {
                    "3Espada", "8Coco", "9Corazon", "3Espada", "2Coco"
                }
            };
            Jugador jugador3 = new Jugador()
            {
                nombre = "Pablo",
                cartas = new List<string>()
                {
                    "3Trebol", "2Corazon", "8Coco", "1Corazon", "6Corazon",
                }
            };
            Jugador jugador4 = new Jugador()
            {
                nombre = "Pablo",
                cartas = new List<string>()
                {
                    "7Espada", "5Coco", "1Corazon", "1Espada", "4Coco"
                }
            };
            Jugador jugador5 = new Jugador()
            {
                nombre = "Pablo",
                cartas = new List<string>()
                {
                    "5Espada", "8Coco", "1Corazon", "1Espada", "2Coco"
                }
            };
            List<Jugador> jugadores = new List<Jugador>();
            jugadores.Add(jugador1);
            jugadores.Add(jugador2);
            jugadores.Add(jugador3);
            jugadores.Add(jugador4);
            jugadores.Add(jugador5);

            Assert.Throws(typeof(Exception), () => prueba1.iniciarJuego(jugadores));
        }
        public void Test4()
        {
            var prueba1 = new ClasePoker();

            Jugador jugador1 = new Jugador()
            {
                nombre = "Pablo",
                cartas = new List<string>()
                {
                    "10Espada", "8Trebol", "10Corazon", "2Espada", "1Coco"
                }
            };
            Jugador jugador2 = new Jugador()
            {
                nombre = "Rodrigo",
                cartas = new List<string>()
                {
                    "3Espada", "8Coco", "9Corazon", "3Espada", "2Coco"
                }
            };
            Jugador jugador3 = new Jugador()
            {
                nombre = "Juan",
                cartas = new List<string>()
                {
                    "3Trebol", "2Corazon", "8Coco", "1Corazon", "6Corazon",
                }
            };
            Jugador jugador4 = new Jugador()
            {
                nombre = "Pablo",
                cartas = new List<string>()
                {
                    "7Espada", "5Coco", "1Corazon", "1Espada", "4Coco"
                }
            };
            Jugador jugador5 = new Jugador()
            {
                nombre = "Luis",
                cartas = new List<string>()
                {
                    "5Tregol", "8Coco", "13Corazon", "13Espada", "11Coco"
                }
            };
            Jugador jugador6 = new Jugador()
            {
                nombre = "Sthefanie",
                cartas = new List<string>()
                {
                    "5Espada", "8Coco", "1Corazon", "1Espada", "2Coco"
                }
            };
            Jugador jugador7 = new Jugador()
            {
                nombre = "F�tima",
                cartas = new List<string>()
                {
                    "5Espada", "8Coco", "1Corazon", "1Espada", "2Coco"
                }
            };
            List<Jugador> jugadores = new List<Jugador>();
            jugadores.Add(jugador1);
            jugadores.Add(jugador2);
            jugadores.Add(jugador3);
            jugadores.Add(jugador4);
            jugadores.Add(jugador5);
            jugadores.Add(jugador6);
            jugadores.Add(jugador7);

            Assert.Throws(typeof(Exception), () => prueba1.iniciarJuego(jugadores));
        }
        public void Test5()
        {
            var prueba1 = new ClasePoker();

            Jugador jugador1 = new Jugador()
            {
                nombre = "Mar�a",
                cartas = new List<string>()
                {
                    "10Espada", "8Trebol", "10Corazon", "2Espada", "1Coco"
                }
            };
            Jugador jugador2 = new Jugador()
            {
                nombre = "Nelson",
                cartas = new List<string>()
                {
                    "3Espada", "8Coco", "9Corazon", "3Espada", "2Coco"
                }
            };
            Jugador jugador3 = new Jugador()
            {
                nombre = "Karolyn",
                cartas = new List<string>()
                {
                    "3Trebol", "2Corazon", "8Coco", "1Corazon", "6Corazon",
                }
            };
            Jugador jugador4 = new Jugador()
            {
                nombre = "Karen",
                cartas = new List<string>()
                {
                    "7Espada", "5Coco", "1Corazon", "1Espada", "4Coco"
                }
            };
            Jugador jugador5 = new Jugador()
            {
                nombre = "Miguel",
                cartas = new List<string>()
                {
                    "5Tregol", "8Coco", "13Corazon", "13Espada", "11Coco"
                }
            };
            List<Jugador> jugadores = new List<Jugador>();
            jugadores.Add(jugador1);
            jugadores.Add(jugador2);
            jugadores.Add(jugador3);
            jugadores.Add(jugador4);
            jugadores.Add(jugador5);

            Assert.Throws(typeof(Exception), () => prueba1.iniciarJuego(jugadores));
        }
        [Test]
        public void Test6()
        {
            var prueba1 = new ClasePoker();

            Jugador jugador1 = new Jugador()
            {
                nombre = "Karol",
                cartas = new List<string>()
                {
                    "10Espada", "8Trebol", "10Corazon", "2Espada", "1Coco"
                }
            };
            Jugador jugador2 = new Jugador()
            {
                nombre = "Loana",
                cartas = new List<string>()
                {
                    "3Espada", "8Coco", "9Corazon", "3Espada", "2Coco"
                }
            };

            List<Jugador> jugadores = new List<Jugador>();
            jugadores.Add(jugador1);
            jugadores.Add(jugador2);

            Assert.Throws(typeof(Exception), () => prueba1.iniciarJuego(jugadores));
        }
        public void Test7()
        {
            var prueba1 = new ClasePoker();
            List<Jugador> jugadores = new List<Jugador>();

            Assert.Throws(typeof(Exception), () => prueba1.iniciarJuego(jugadores));
        }
        //Los casos de prueba deben
        //validar que no existan cartas iguales en una partida
        //(usar exceptions)
        [Test]
        public void Test8()
        {
            var prueba1 = new ClasePoker();

            List<String> cartas = new List<String>()
            {
                "1Corazon","2Corazon","3Corazon","4Corazon","5Corazon","6Corazon","7Corazon",
                "8Corazon","9Corazon","10Corazon","JotaCorazon","ReynaCorazon","ReyCorazon",
                "1Cocos","2Cocos","3Cocos","4Cocos","5Cocos","6Cocos","7Cocos","8Cocos",
                "9Cocos","10Cocos","JotaCocos","ReynaCocos","ReyCocos",
                "1Trebol","2Trebol","3Trebol","4Trebol","5Trebol","6Trebol","7Trebol",
                "8Trebol","9Trebol","10Trebol","JotaTrebol","ReynaTrebol","ReyTrebol",
                "1Espadas","2Espadas","3Espadas","4Espadas","5Espadas","6Espadas","7Espadas",
                "8Espadas","9Espadas","10Espadas","JotaEspadas","ReynaEspadas","ReyEspadas"
            };

            Assert.Throws(typeof(Exception), () => prueba1.verificarBaraja(cartas));
        }
        [Test]
        public void Test9()
        {
            var prueba1 = new ClasePoker();

            List<String> cartas = new List<String>()
            {
                "1Corazon","2Corazon","3Corazon","4Corazon","5Corazon","6Corazon","7Corazon",
                "8Corazon","9Corazon","10Corazon","JotaCorazon","ReynaCorazon","ReyCorazon",
                "1Cocos","2Cocos","3Cocos","4Cocos","5Cocos","6Cocos","7Cocos","8Cocos",
                "9Cocos","10Cocos","JotaCocos","ReynaCocos","ReyCocos",
                "1Trebol","2Trebol","3Trebol","ReynaCocos","5Trebol","6Trebol","7Trebol",
                "8Trebol","9Trebol","10Trebol","JotaTrebol","ReynaTrebol","ReyTrebol",
                "1Espadas","2Espadas","3Espadas","4Espadas","5Espadas","6Espadas","7Espadas",
                "8Espadas","9Espadas","10Espadas","JotaEspadas","ReynaEspadas","ReyEspadas"
            };

            Assert.Throws(typeof(Exception), () => prueba1.verificarBaraja(cartas));
        }
        [Test]
        public void Test10()
        {
            var prueba1 = new ClasePoker();

            List<String> cartas = new List<String>()
            {
                "1Corazon","2Corazon","3Corazon","4Corazon","5Corazon","6Corazon","7Corazon",
                "8Corazon","9Corazon","7Trebol","JotaCorazon","ReynaCorazon","ReyCorazon",
                "1Cocos","2Cocos","3Cocos","4Cocos","5Cocos","6Cocos","7Cocos","8Cocos",
                "9Cocos","10Cocos","JotaCocos","ReynaCocos","ReyCocos",
                "1Trebol","2Trebol","3Trebol","ReynaCocos","5Trebol","6Trebol","7Trebol",
                "8Trebol","9Trebol","10Trebol","JotaTrebol","ReynaTrebol","ReyTrebol",
                "1Espadas","2Espadas","3Espadas","4Espadas","ReyCocos","6Espadas","7Espadas",
                "8Espadas","9Espadas","10Espadas","JotaEspadas","ReynaEspadas","ReyEspadas"
            };

            Assert.Throws(typeof(Exception), () => prueba1.verificarBaraja(cartas));
        }
        [Test]
        public void Test11()
        {
            var prueba1 = new ClasePoker();

            List<String> cartas = new List<String>()
            {
                "1Corazon","2Corazon","3Corazon","4Corazon","5Corazon","6Corazon","8Trebol",
                "8Corazon","9Corazon","7Trebol","JotaCorazon","ReynaCorazon","ReyCorazon",
                "1Cocos","2Cocos","3Cocos","4Cocos","5Cocos","6Cocos","7Cocos","8Cocos",
                "9Cocos","10Cocos","JotaCocos","ReynaCocos","ReyCocos",
                "1Trebol","2Trebol","3Trebol","ReynaCocos","5Trebol","6Trebol","7Trebol",
                "8Trebol","9Trebol","10Trebol","JotaTrebol","JokerBN","ReynaTrebol","ReyTrebol",
                "1Espadas","2Espadas","3Espadas","4Espadas","ReyCocos","6Espadas","7Espadas",
                "8Espadas","9Espadas","10Espadas","JotaEspadas","ReynaEspadas","ReyEspadas",
                "JokerBN","JokerAmarillo"
            };

            Assert.Throws(typeof(Exception), () => prueba1.verificarBaraja(cartas));
        }
        [Test]
        public void Test12()
        {
            var prueba1 = new ClasePoker();

            List<String> cartas = new List<String>()
            {
                "1Corazon","2Corazon","3Corazon","4Corazon","5Corazon","6Corazon","7Corazon",
                "8Corazon","9Corazon","7Trebol","JotaCorazon","ReynaCorazon","ReyCorazon",
                "1Cocos","2Cocos","3Cocos","4Cocos","5Cocos","6Cocos","7Cocos","8Cocos",
                "9Cocos","10Cocos","JotaCocos","ReynaCocos","ReyCocos",
                "1Trebol","2Trebol","3Trebol","ReynaCocos","5Trebol","6Trebol","7Trebol",
                "8Trebol","9Trebol","1Espadas","2Espadas","3Espadas","4Espadas","ReyCocos","6Espadas","7Espadas",
                "8Espadas","9Espadas","10Espadas","JotaEspadas","ReynaEspadas","ReyEspadas"
            };

            Assert.Throws(typeof(Exception), () => prueba1.verificarBaraja(cartas));
        }
        [Test]
        public void Test13()
        {
            var prueba1 = new ClasePoker();

            List<String> cartas = new List<String>()
            {
                "1Corazon","2Corazon","3Corazon","ReynaEspadas","5Corazon","6Corazon","7Corazon",
                "8Corazon","9Corazon","7Trebol","JotaCorazon","ReynaCorazon","ReyCorazon",
                "1Cocos","2Cocos","3Cocos","4Cocos","5Cocos","6Cocos","7Cocos","8Cocos",
                "9Cocos","10Cocos","JotaCocos","ReynaCocos","ReyCocos",
                "1Trebol","2Trebol","3Trebol","ReynaCocos","5Trebol","6Trebol","7Trebol",
                "8Trebol","9Trebol","10Trebol","JotaTrebol","ReynaTrebol","ReyTrebol",
                "1Espadas","2Espadas","3Espadas","4Espadas","ReyCocos","6Espadas","7Espadas",
                "8Espadas","9Espadas","10Espadas","JotaEspadas","ReynaEspadas","ReyEspadas",
                "JokerBN","JokerAmarillo"
            };

            Assert.Throws(typeof(Exception), () => prueba1.verificarBaraja(cartas));

            Assert.Pass();
        }
        [Test]
        public void Test14()
        {
            var prueba1 = new ClasePoker();

            List<String> cartas = new List<String>()
            {
                "1Corazon","2Corazon","3Corazon","4Corazon","5Corazon","6Corazon","7Corazon",
                "8Corazon","9Corazon","10Corazon","JotaCorazon","ReynaCorazon","ReyCorazon",
                "1Cocos","2Cocos","3Cocos","4Cocos","5Cocos","6Cocos","7Cocos","8Cocos",
                "9Cocos","10Cocos","JotaCocos","ReynaCocos","ReyCocos",
                "1Trebol","2Trebol","3Trebol","4Trebol","5Trebol","6Trebol","7Trebol",
                "8Trebol","9Trebol","10Trebol","JotaTrebol","ReynaTrebol","ReyTrebol",
                "1Espadas","2Espadas","3Espadas","4Espadas","5Espadas","6Espadas","7Espadas",
                "8Espadas","9Espadas","10Espadas","JotaEspadas","ReynaEspadas","ReyEspadas",
                "JokerBN","JokerAmarillo"
            };

            Assert.Throws(typeof(Exception), () => prueba1.verificarBaraja(cartas));
        }
        [Test]
        public void Test15()
        {
            var prueba1 = new ClasePoker();

            List<String> cartas = new List<String>()
            {
                "AsCorazon","2Corazon","3Corazon","4Corazon","5Corazon","6Corazon","7Corazon",
                "8Corazon","9Corazon","10Corazon","JotaCorazon","ReynaCorazon","ReyCorazon",
                "AsCocos","2Cocos","3Cocos","4Cocos","5Cocos","6Cocos","7Cocos","8Cocos",
                "9Cocos","10Cocos","JotaCocos","ReynaCocos","ReyCocos",
                "AsTrebol","2Trebol","3Trebol","4Trebol","5Trebol","6Trebol","7Trebol",
                "8Trebol","9Trebol","10Trebol","JotaTrebol","ReynaTrebol","ReyTrebol",
                "AsEspadas","2Espadas","3Espadas","4Espadas","5Espadas","6Espadas","7Espadas",
                "8Espadas","9Espadas","10Espadas","JotaEspadas","ReynaEspadas","ReyEspadas",
                "JokerBN"
            };

            Assert.Throws(typeof(Exception), () => prueba1.verificarBaraja(cartas));
        }
        //verficia que cartas no se repitan, solo que verfica dos listas,
        //las cartas ya tiradas , y las que est�n en la mesa
        [Test]
        public void Test16()
        {
            var prueba1 = new ClasePoker();

            List<String> cartasTiras = new List<String>()
            {
                "6Corazon","4Cocos","1Corazon","2Corazon","JotaEspadas",
                "8Corazon","9Corazon","10Corazon","JotaCorazon",
                "AsEspadas","2Espadas","4Espadas","5Espadas","6Espadas","7Espadas",
                "5Corazon","7Corazon","10Espadas","3Espadas"
            };
            List<string> cartasMesa = new List<string>()
            {
                "JotaCorazon","ReyTrebol","Astrebol","3cocos","8Corazon"
            };

            Assert.Throws(typeof(Exception), () => prueba1.verificarBarajaTiradas(cartasMesa, cartasTiras));
        }
        [Test]
        public void Test17()
        {
            var prueba1 = new ClasePoker();

            List<String> cartasTiras = new List<String>()
            {
                "6Corazon","4Cocos","1Corazon","2Corazon","JotaEspadas",
                "6Espadas","9Corazon","ReynaCocos","JotaCorazon",
                "AsEspadas","2Espadas","4Espadas","5Espadas","6Espadas","8Espadas",
                "5Corazon","7Corazon","10Espadas","3Espadas"
            };
            List<string> cartasMesa = new List<string>()
            {
                "JotaCorazon","ReyTrebol","6Espadas","3cocos","8Corazon"
            };

            Assert.Throws(typeof(Exception), () => prueba1.verificarBarajaTiradas(cartasMesa, cartasTiras));
        }
        //cambiar
        [Test]
        public void Test18()
        {
            var prueba1 = new ClasePoker();

            List<String> cartasTiras = new List<String>()
            {
                "6Corazon","4Cocos","1Corazon","2Corazon","JotaEspadas",
                "6Espadas","9Corazon","ReynaCocos","JotaCorazon",
                "AsEspadas","2Espadas","4Espadas","5Espadas","6Espadas","8Espadas",
                "5Corazon","7Corazon","10Espadas","3Espadas"
            };
            List<string> cartasMesa = new List<string>()
            {
                "JotaCorazon","ReyTrebol","6Espadas","3cocos","8Corazon"
            };

            Assert.Throws(typeof(Exception), () => prueba1.verificarBarajaTiradas(cartasMesa, cartasTiras));
        }
        [Test]
        public void Test19()
        {
            var prueba1 = new ClasePoker();

            List<String> cartasTiras = new List<String>()
            {
                "6Corazon","4Cocos","1Corazon","2Corazon","JotaEspadas",
                "6Espadas","9Corazon","5Espadas","6Espadas","3cocos","8Corazon",
                "10Espadas","3Espadas","5Espadas","6Espadas","8Espadas",
                "4Espadas","5Espadas","6Espadas","8Espadas",
            };
            List<string> cartasMesa = new List<string>()
            {
                "ReynaEspadas","ReyEspadas","6Espadas","3Cocos","10Cocos"
            };

            Assert.Throws(typeof(Exception), () => prueba1.verificarBarajaTiradas(cartasMesa, cartasTiras));
        }
        [Test]
        public void Test20()
        {
            var prueba1 = new ClasePoker();

            List<String> cartasTiras = new List<String>()
            {
                "6Corazon","4Cocos","1Corazon","2Corazon","JotaEspadas",
                "6Espadas","9Corazon","10Trebol","JotaTrebol","ReynaTrebol",
                "5Espadas","6Espadas","3cocos","8Corazon",
                "10Espadas","3Espadas","5Espadas","6Espadas","8Espadas",
                "5Corazon","7Corazon","10Espadas","3Espadas"
            };
            List<string> cartasMesa = new List<string>()
            {
                "JotaCorazon","6Corazon","6Espadas","9cocos","8Trebol"
            };

            Assert.Throws(typeof(Exception), () => prueba1.verificarBarajaTiradas(cartasMesa, cartasTiras));
        }
        //pruebas de ganador
        [Test]
        public void Test21()
        {
            Jugador jugador1 = new Jugador()
            {
                nombre = "Analiz",
                cartas = new List<string>()
                {
                    "AsTrebol", "ReyTrebol", "ReynaTrebol", "JotaTrebol", "10Trebol"
                }
            };
            Jugador jugador2 = new Jugador()
            {
                nombre = "Gissela",
                cartas = new List<string>()
                {
                    "8Trebol", "8Trebol", "5Trebol", "JotaCorazon", "10Cocos"
                }
            };
            List<Jugador> jugadores = new List<Jugador>();
            jugadores.Add(jugador1);
            jugadores.Add(jugador2);
            var prueba = new ClasePoker();
            prueba.iniciarJuego(jugadores);
            Assert.AreEqual("Analiz", prueba.ganadorJuego());
        }
        //pruebas de ganador
        [Test]
        public void Test22()
        {
            Jugador jugador1 = new Jugador()
            {
                nombre = "Nataly",
                cartas = new List<string>()
                {
                    "AsTrebol", "ReyTrebol", "ReynaTrebol", "JotaTrebol", "10Trebol"
                }
            };
            Jugador jugador2 = new Jugador()
            {
                nombre = "Pablo",
                cartas = new List<string>()
                {
                    "8Trebol", "8Trebol", "5Trebol", "JotaCorazon", "10Cocos"
                }
            };
            List<Jugador> jugadores = new List<Jugador>();
            jugadores.Add(jugador1);
            jugadores.Add(jugador2);
            var prueba = new ClasePoker();
            prueba.iniciarJuego(jugadores);
            Assert.AreEqual("Nataly", prueba.ganadorJuego());
        }
        [Test]
        public void Test23()
        {
            Jugador jugador1 = new Jugador()
            {
                nombre = "F�tima",
                cartas = new List<string>()
                {
                    "AsTrebol", "3Trebol", "10Trebol", "JotaTrebol", "4Trebol"
                }
            };
            Jugador jugador2 = new Jugador()
            {
                nombre = "Mar�a",
                cartas = new List<string>()
                {
                    "8Corazon", "8Trebol", "8Cocos", "8Espadas", "Joker"
                }
            };
            List<Jugador> jugadores = new List<Jugador>();
            jugadores.Add(jugador1);
            jugadores.Add(jugador2);
            var prueba = new ClasePoker();
            prueba.iniciarJuego(jugadores);
            Assert.AreEqual("F�tima", prueba.ganadorJuego());
        }
        [Test]
        public void Test24()
        {
            Jugador jugador1 = new Jugador()
            {
                nombre = "Juan",
                cartas = new List<string>()
                {
                    "7Trebol", "8Trebol", "9Trebol", "JotaTrebol", "10Trebol"
                }
            };
            Jugador jugador2 = new Jugador()
            {
                nombre = "Pedro",
                cartas = new List<string>()
                {
                    "8Trebol", "4Trebol", "5Trebol", "JotaCorazon", "10Cocos"
                }
            };
            List<Jugador> jugadores = new List<Jugador>();
            jugadores.Add(jugador1);
            jugadores.Add(jugador2);
            var prueba = new ClasePoker();
            prueba.iniciarJuego(jugadores);
            Assert.AreEqual("Juan", prueba.ganadorJuego());
        }
        [Test]
        public void Test25()
        {
            Jugador jugador1 = new Jugador()
            {
                nombre = "Pablo",
                cartas = new List<string>()
                {
                    "AsCocos", "ReyCorazon", "ReynaTrebol", "ReyTrebol", "3Trebol"
                }
            };
            Jugador jugador2 = new Jugador()
            {
                nombre = "Juan",
                cartas = new List<string>()
                {
                    "4Trebol", "6Trebol", "8Cocos", "JotaCorazon", "ReynaCocos"
                }
            };
            Jugador jugador3 = new Jugador()
            {
                nombre = "Luis",
                cartas = new List<string>()
                {
                    "AsCorazon", "8Trebol", "5Trebol", "7Corazon", "10Trebol"
                }
            };
            Jugador jugador4 = new Jugador()
            {
                nombre = "Sthefanie",
                cartas = new List<string>()
                {
                    "AsEspadas", "5Corazon", "9Trebol", "3Corazon", "10Cocos"
                }
            };
            Jugador jugador5 = new Jugador()
            {
                nombre = "Cristina",
                cartas = new List<string>()
                {
                    "10Espadas", "2Cocos", "7Trebol", "9Corazon", "JotaCocos"
                }
            };
            List<Jugador> jugadores = new List<Jugador>();
            jugadores.Add(jugador1);
            jugadores.Add(jugador2);
            jugadores.Add(jugador3);
            jugadores.Add(jugador4);
            jugadores.Add(jugador5);

            var prueba = new ClasePoker();
            prueba.iniciarJuego(jugadores);
            prueba.retirarJugador(jugador5);
            prueba.retirarJugador(jugador3);
            prueba.retirarJugador(jugador4);
            prueba.retirarJugador(jugador2);
            Assert.AreEqual("Pablo", prueba.ganadorJuego());
        }
        [Test]
        public void Test26()
        {
            Jugador jugador1 = new Jugador()
            {
                nombre = "Pablo",
                cartas = new List<string>()
                {
                    "AsCocos", "4Corazon", "ReynaTrebol", "4Trebol", "reyTrebol"
                }
            };
            Jugador jugador2 = new Jugador()
            {
                nombre = "Sthefanie",
                cartas = new List<string>()
                {
                    "ReynaTrebol", "6Trebol", "ReyCocos", "ReynaCorazon", "ReynaCocos"
                }
            };
            Jugador jugador3 = new Jugador()
            {
                nombre = "Luis",
                cartas = new List<string>()
                {
                    "AsCorazon", "8Trebol", "5Trebol", "7Corazon", "10Trebol"
                }
            };
            Jugador jugador4 = new Jugador()
            {
                nombre = "Sthefanie",
                cartas = new List<string>()
                {
                    "AsEspadas", "5Corazon", "9Trebol", "3Corazon", "10Cocos"
                }
            };
            Jugador jugador5 = new Jugador()
            {
                nombre = "Cristina",
                cartas = new List<string>()
                {
                    "10Espadas", "2Cocos", "7Trebol", "9Corazon", "JotaCocos"
                }
            };
            List<Jugador> jugadores = new List<Jugador>();
            jugadores.Add(jugador1);
            jugadores.Add(jugador2);
            jugadores.Add(jugador3);
            jugadores.Add(jugador4);
            jugadores.Add(jugador5);

            var prueba = new ClasePoker();
            prueba.iniciarJuego(jugadores);
            prueba.retirarJugador(jugador5);
            prueba.retirarJugador(jugador2);
            Assert.AreEqual("Sthefanie", prueba.ganadorJuego());
        }
        //Empata
        [Test]
        public void Test27()
        {
            Jugador jugador1 = new Jugador()
            {
                nombre = "Pablo",
                cartas = new List<string>()
                {
                    "2Cocos", "4Corazon"
                }
            };
            Jugador jugador2 = new Jugador()
            {
                nombre = "Sthefanie",
                cartas = new List<string>()
                {
                    "8Cocos","3Trebol"
                }
            };
            Jugador jugador3 = new Jugador()
            {
                nombre = "Luis",
                cartas = new List<string>()
                {
                    "AsCorazon", "8Trebol", "5Trebol", "7Corazon", "10Trebol"
                }
            };
            Jugador jugador4 = new Jugador()
            {
                nombre = "Sthefanie",
                cartas = new List<string>()
                {
                    "AsEspadas", "5Corazon", "9Trebol", "3Corazon", "10Cocos"
                }
            };
            Jugador jugador5 = new Jugador()
            {
                nombre = "Cristina",
                cartas = new List<string>()
                {
                    "10Espadas", "9Cocos", "7Trebol", "9Corazon", "JotaCocos"
                }
            };
            List<Jugador> jugadores = new List<Jugador>();
            List<string> cartasComunitarias = new List<string>()
            {
                "3Trebol","4Trebol","8Espadas","2Corazon"
            };

            jugadores.Add(jugador1);
            jugadores.Add(jugador2);
            jugadores.Add(jugador3);
            jugadores.Add(jugador4);
            jugadores.Add(jugador5);

            var prueba = new ClasePoker();
            prueba.iniciarJuego(jugadores);
            prueba.retirarJugador(jugador5);
            prueba.retirarJugador(jugador2);
            prueba.cartasComunitarias(cartasComunitarias);
            Assert.AreEqual("Empate", prueba.ganadorJuego());
        }
        //Empata
        [Test]
        public void Test28()
        {
            Jugador jugador1 = new Jugador()
            {
                nombre = "Pablo",
                cartas = new List<string>()
                {
                    "6Cocos", "5Corazones","5Espadas"//5Cocos 6Espadas
                }
            };
            Jugador jugador2 = new Jugador()
            {
                nombre = "Sthefanie",
                cartas = new List<string>()
                {
                    "6Corazones","5Trebol","6Trebol"//6Espadas 5Trebol
                }
            };
            Jugador jugador3 = new Jugador()
            {
                nombre = "Luis",
                cartas = new List<string>()
                {
                    "AsCorazon", "8Trebol", "10Trebol", "7Corazon", "7Trebol"
                }
            };
            Jugador jugador4 = new Jugador()
            {
                nombre = "Sthefanie",
                cartas = new List<string>()
                {
                    "AsEspadas", "9Corazon", "9Trebol", "3Corazon", "10Cocos"
                }
            };
            Jugador jugador5 = new Jugador()
            {
                nombre = "Cristina",
                cartas = new List<string>()
                {
                    "10Espadas", "9Cocos", "7Trebol", "9Corazon", "JotaCocos"
                }
            };
            List<Jugador> jugadores = new List<Jugador>();
            List<string> cartasComunitarias = new List<string>()
            {
                "5Cocos","10Cocos","6Espadas","5Trebol"
            };

            jugadores.Add(jugador1);
            jugadores.Add(jugador2);
            jugadores.Add(jugador3);
            jugadores.Add(jugador4);
            jugadores.Add(jugador5);

            var prueba = new ClasePoker();
            prueba.iniciarJuego(jugadores);
            prueba.retirarJugador(jugador5);
            prueba.retirarJugador(jugador2);
            prueba.cartasComunitarias(cartasComunitarias);
            Assert.AreEqual("Empate", prueba.ganadorJuego());
        }
        //empata
        [Test]
        public void Test29()
        {
            Jugador jugador1 = new Jugador()
            {
                nombre = "Pablo",
                cartas = new List<string>()
                {
                    "9Cocos", "5Corazones","3Espadas"//5Cocos 6Espadas
                }
            };
            Jugador jugador2 = new Jugador()
            {
                nombre = "Sthefanie",
                cartas = new List<string>()
                {
                    "9Corazones","5Trebol","6Trebol"//6Espadas 5Trebol
                }
            };
            
            List<Jugador> jugadores = new List<Jugador>();
            List<string> cartasComunitarias = new List<string>()
            {
                "9Espadas","9Trebol","2Espadas","5Trebol"
            };

            jugadores.Add(jugador1);
            jugadores.Add(jugador2);

            var prueba = new ClasePoker();
            prueba.iniciarJuego(jugadores);
            
            prueba.cartasComunitarias(cartasComunitarias);
            Assert.AreEqual("Empate", prueba.ganadorJuego());
        }
        //gana
        [Test]
        public void Test30()
        {
            Jugador jugador1 = new Jugador()
            {
                nombre = "Pablo",
                cartas = new List<string>()
                {
                    "3Cocos", "4Corazones","5Espadas","6Cocos","7Cocos"//5Cocos 6Espadas
                }
            };
            Jugador jugador2 = new Jugador()
            {
                nombre = "Sthefanie",
                cartas = new List<string>()
                {
                    "8Corazones","9Trebol","10Trebol","JotaTrebol","ReynaCorazon"//6Espadas 5Trebol
                }
            };

            List<Jugador> jugadores = new List<Jugador>();
            List<string> cartasComunitarias = new List<string>()
            {
                "ReyCorazon","9Corazon","2Espadas","5Trebol"
            };

            jugadores.Add(jugador1);
            jugadores.Add(jugador2);

            var prueba = new ClasePoker();
            prueba.iniciarJuego(jugadores);

            prueba.cartasComunitarias(cartasComunitarias);
            Assert.AreEqual("Sthefanie", prueba.ganadorJuego());
        }
    }
}